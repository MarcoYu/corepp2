package testoperaciones;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import model.CalculadoraBasica;
import model.Operaciones;

public class AllTest {
	private Operaciones op;
	
	private CalculadoraBasica cal;
	
	@Before
	public void setup() {
		op = new Operaciones(2,3);
		cal = new CalculadoraBasica(5, 6);
	}
	
	
	
	@Test
	public void sumarTest()
	{
		assertEquals(5, op.sumar());
	}

	@Test
	public void multiplicarTest()
	{
		assertEquals(30, cal.multiplicar());
	}


}
