package model;

public class CalculadoraBasica {

	private int num1, num2;
	
	public CalculadoraBasica(int n, int m) {
		num1 = n;
		num2 = m;
	}
	
	public int multiplicar() {
		return num1*num2;
	}
}
